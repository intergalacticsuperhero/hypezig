package com.kolloware.hypezigapp.db;

import android.content.Context;
import android.util.Log;

import com.kolloware.hypezigapp.models.Event;

import java.util.Date;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;

public class DatabaseUtils {

    public static void insertOrUpdateAllEvents(List<Event> inEvents, Context inContext) {
        Log.d(LOG_DATA, DatabaseUtils.class.getSimpleName()
                + ".insertOrUpdateAllEvents() called with: inEvents = [" + inEvents.size() + "], inContext = [" + inContext + "]");

        AppDatabase db = AppDatabase.getInstance(inContext);

        for (Event forEvent : inEvents) {
            Event oldEvent = db.eventDao().getByProviderNameAndId(forEvent.providerName,
                    forEvent.providerId);

            if (oldEvent == null) {
                Log.v(LOG_DATA, "Insert new event: " + forEvent);
                db.eventDao().insertAll(forEvent);
            }
            else {
                Log.v(LOG_DATA, "Update event: " + forEvent);
                oldEvent.title = forEvent.title;
                oldEvent.subtitle = forEvent.subtitle;
                oldEvent.details = forEvent.details;
                oldEvent.date = forEvent.date;
                oldEvent.tags = forEvent.tags;
                oldEvent.imageURL = forEvent.imageURL;
                oldEvent.category = forEvent.category;
                oldEvent.locationName = forEvent.locationName;
                oldEvent.locationURL = forEvent.locationURL;
                db.eventDao().update(oldEvent);
            }
        }
    }


    public static void hideDuplicates(Context inContext) {
        Log.d(LOG_DATA, DatabaseUtils.class.getSimpleName() + ".hideDuplicates() called with: inContext = [" + inContext + "]");

        AppDatabase db = AppDatabase.getInstance(inContext);
        List<Event> allEvents = db.eventDao().getCurrentEvents((new Date()).getTime());

        for (int i = 0; i < allEvents.size() - 1; i++) {
            Event firstEvent = allEvents.get(i);

            if (firstEvent.duplicate) continue;

            for (int j = i + 1; j < allEvents.size(); j++) {
                Event secondEvent = allEvents.get(j);

                if (secondEvent.duplicate) continue;

                if (firstEvent.date.compareTo(secondEvent.date) == 0) {
                    double titleSimilarity = similarity(firstEvent.title, secondEvent.title);
                    double venueSimilarity = similarity(firstEvent.locationName, secondEvent.locationName);

                    if (titleSimilarity > 0.5 && venueSimilarity > 0.5) {
                        double qualityFirstEvent = getDataQuality(firstEvent);
                        double qualitySecondEvent = getDataQuality(secondEvent);

                        Log.d(LOG_DATA, "firstEvent: " + firstEvent);
                        Log.d(LOG_DATA, "1. quality: " + qualityFirstEvent);
                        Log.d(LOG_DATA, "secondEvent: " + secondEvent);
                        Log.d(LOG_DATA, "2. quality: " + qualitySecondEvent);

                        if (qualitySecondEvent > qualityFirstEvent) {
                            Log.d(LOG_DATA, "Marking first event as duplicate");
                            firstEvent.duplicate = true;
                            db.eventDao().update(firstEvent);
                            break;
                        }
                        else {
                            Log.d(LOG_DATA, "Marking second event as duplicate");
                            secondEvent.duplicate = true;
                            db.eventDao().update(secondEvent);
                        }
                    }
                }
                else {
                    break;
                }
            }
        }
    }

    private static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) {
            longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0;  }

        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
    }

    private static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

    private static double getDataQuality(Event e) {
        double points = 0;

        if (e.imageURL != null && e.imageURL.length() > 0) { points += 10; }
        if (e.details != null) {
            points += (e.details.length() / 50.0);
        }
        if (e.tags != null) {
            points += e.tags.size();
        }

        return points;
    }
}
