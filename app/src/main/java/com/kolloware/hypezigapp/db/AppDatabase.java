package com.kolloware.hypezigapp.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

@Database(entities = {Event.class, Downloader.class}, version = 6)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "hypezig-events";

    private static AppDatabase instance;

    public abstract EventDao eventDao();
    public abstract DownloaderDao downloaderDao();

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("UPDATE event SET category = NULL");
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `Downloader` (`downloaderId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `progress` INTEGER NOT NULL, `running` INTEGER NOT NULL, `lastUpdate` INTEGER, `result` TEXT, `scraperType` TEXT)");
        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Event` ADD COLUMN `duplicate` INTEGER NOT NULL DEFAULT 0");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Event` ADD COLUMN `downloaderId` INTEGER");
        }
    };

    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Downloader` ADD COLUMN `properties` TEXT");
        }
    };

    public static AppDatabase getInstance(Context context) {
        if (instance == null) instance = Room.databaseBuilder(context, AppDatabase.class,
                DATABASE_NAME).addMigrations(MIGRATION_1_2,
                MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6).build();
        return instance;
    }
}
