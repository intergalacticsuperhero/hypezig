package com.kolloware.hypezigapp.db;

import androidx.room.TypeConverter;

import com.kolloware.hypezigapp.models.ScraperType;

public class ScraperTypeConverter {

    @TypeConverter
    public static ScraperType toScraperType(String scraperTypeString){
        if (scraperTypeString == null) return null;
        return ScraperType.valueOf(scraperTypeString);
    }

    @TypeConverter
    public static String fromScraperType(ScraperType scraperType){
        if (scraperType == null) return null;
        return scraperType.toString();
    }
}
