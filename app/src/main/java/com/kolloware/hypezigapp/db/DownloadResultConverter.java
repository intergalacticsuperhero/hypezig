package com.kolloware.hypezigapp.db;

import androidx.room.TypeConverter;

import com.kolloware.hypezigapp.models.DownloadStatus;

public class DownloadResultConverter {

    @TypeConverter
    public static DownloadStatus toDownloadResult(String downloadResultString){
        if (downloadResultString == null) return null;
        return DownloadStatus.valueOf(downloadResultString);
    }

    @TypeConverter
    public static String fromDownloadResult(DownloadStatus downloadStatus){
        if (downloadStatus == null) return null;
        return downloadStatus.toString();
    }
}
