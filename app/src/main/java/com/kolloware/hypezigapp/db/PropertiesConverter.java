package com.kolloware.hypezigapp.db;

import android.util.Log;

import androidx.room.TypeConverter;

import com.kolloware.hypezigapp.models.Category;

import org.json.JSONObject;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;

public class PropertiesConverter {

    @TypeConverter
    public static JSONObject toProperties(String propertiesString){
        if (propertiesString == null) return new JSONObject();

        try {
            return new JSONObject(propertiesString);
        }
        catch (Exception e) {
            Log.e(LOG_DATA, "Error: " + e.getMessage());
            Log.e(LOG_DATA, Log.getStackTraceString(e));
        }

        return null;
    }

    @TypeConverter
    public static String fromProperties(JSONObject properties){
        if (properties == null) properties = new JSONObject();
        return properties.toString();
    }
}
