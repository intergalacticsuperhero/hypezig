package com.kolloware.hypezigapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.kolloware.hypezigapp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kolloware.hypezigapp.models.Model;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment homeFragment = new HomeFragment();
    private Fragment favoritesFragment = new FavoritesFragment();
    private Fragment downloadsFragment = new DownloadsFragment();

    private DrawerLayout drawer;
    private BottomNavigationView bottomNav;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int selectedNavigationFragmentId = Model.getInstance().getMainActivityNavigationFragmentId();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        bottomNav.setSelectedItemId(selectedNavigationFragmentId);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                getFragmentByNavigationId(selectedNavigationFragmentId)).commit();

        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_home);
        }

        setTitle(R.string.app_name);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        getFragmentByNavigationId(R.id.nav_home)).commit();
                bottomNav.setSelectedItemId(R.id.nav_home);
                break;
            case R.id.nav_favorites:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        getFragmentByNavigationId(R.id.nav_favorites)).commit();
                bottomNav.setSelectedItemId(R.id.nav_favorites);
                break;
            case R.id.nav_download:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        getFragmentByNavigationId(R.id.nav_download)).commit();
                bottomNav.setSelectedItemId(R.id.nav_download);
                break;
            case R.id.nav_about:
                Intent aboutIntent = new Intent(this, AboutActivity.class);
                this.startActivity(aboutIntent);
                break;
            case R.id.nav_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                this.startActivity(settingsIntent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    private Fragment getFragmentByNavigationId(int navigationId) {
        switch (navigationId) {
            case R.id.nav_favorites:
                return favoritesFragment;
            case R.id.nav_download:
                return downloadsFragment;
            default:
                return homeFragment;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Log.i(LOG_UI, "Option '" + menuItem.toString() + "' selected");

                    Fragment selectedFragment = getFragmentByNavigationId(menuItem.getItemId());
                    Model.getInstance().setMainActivityNavigationFragmentId(menuItem.getItemId());

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();
                    navigationView.setCheckedItem(menuItem.getItemId());

                    return true;
                }
            };

    @Override
    public void onAttachFragment(Fragment fragment){
        Log.i(LOG_UI, "Fragment " + fragment.getClass().getSimpleName() + " attached to "
                + this.getClass().getSimpleName()  );
    }
}
