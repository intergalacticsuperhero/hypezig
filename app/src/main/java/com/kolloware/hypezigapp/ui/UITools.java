package com.kolloware.hypezigapp.ui;

import android.content.Context;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Event;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UITools {

    private static UITools instance;

    private final Map<String, Category> categoryByLabel;

    private UITools(Context inContext) {
        // Init mapping
        categoryByLabel = new HashMap<>();

        for (Map.Entry<Category, Integer> entry : RESOURCE_KEY_BY_CATEGORY.entrySet()) {
            categoryByLabel.put(inContext.getString(entry.getValue()),
                    entry.getKey());
        }
    }

    public static UITools getInstance(Context inContext) {
        if (instance == null) {
            instance = new UITools(inContext);
        }

        return instance;
    }

    public static final Map<Category, Integer> RESOURCE_KEY_BY_CATEGORY = new HashMap<Category, Integer>(){{
        put(Category.ART, R.string.category_label_art);
        put(Category.CINEMA, R.string.category_label_cinema);
        put(Category.CITY, R.string.category_label_city);
        put(Category.CULTURE, R.string.category_label_culture);
        put(Category.FAMILY, R.string.category_label_family);
        put(Category.FOOD, R.string.category_label_food);
        put(Category.GUIDED_TOUR, R.string.category_label_guided_tour);
        put(Category.LECTURES, R.string.category_label_lectures);
        put(Category.MISC, R.string.category_label_misc);
        put(Category.MUSIC, R.string.category_label_music);
        put(Category.PARTY, R.string.category_label_party);
        put(Category.SHOPPING, R.string.category_label_shopping);
        put(Category.SPORTS, R.string.category_label_sports);
        put(Category.STAGE, R.string.category_label_stage);
    }};

    public static int getCategoryResourceId(Event inEvent) {
        Integer localResult = RESOURCE_KEY_BY_CATEGORY.get(inEvent.category);

        if (localResult != null) {
            return localResult;
        }
        else {
            return R.string.category_label_misc;
        }
    }

    public Map<String, Category> getCategoryByLabelMap() {
        return categoryByLabel;
    }

    public Set<Category> getCategoriesBySearchTerms(String inSearchTerm) {
        Set<Category> localResult = new HashSet<>();

        for (Map.Entry<String, Category> forEntry : categoryByLabel.entrySet()) {
            if (forEntry.getKey().toLowerCase().contains(inSearchTerm)) {
                localResult.add(forEntry.getValue());
            }
        }

        return localResult;
    }
}
