package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.models.ScraperTypeInformation;
import com.kolloware.hypezigapp.tasks.InsertDownloaderTask;

import org.json.JSONObject;

import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;

public class CreateDownloaderDialog extends AbstractDownloaderDialog {

    private TextInputLayout editTextTitle, editTextURL;
    private TextView textViewScraperType;

    public CreateDownloaderDialog(ScraperTypeInformation inScraperType, DownloadsRecyclerViewAdapter adapter) {
        super(inScraperType, adapter);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_downloader_editmask, null);

        builder.setView(view)
                .setTitle("Hinzufügen")
                .setNegativeButton("cancel", null)
                .setPositiveButton("ok", null);

        textViewScraperType = view.findViewById(R.id.scraper_type);
        editTextTitle = view.findViewById(R.id.text_input_title);
        editTextURL = view.findViewById(R.id.text_input_url);

        if ((scraperTypeInformation.scraperType != ScraperType.ICAL)
                && (scraperTypeInformation.scraperType != ScraperType.FACEBOOK_PAGE)) {
            editTextURL.setVisibility(View.GONE);
        }

        textViewScraperType.setText(getString(R.string.edit_downloader_calendar_type) + ": "
                + scraperTypeInformation.scraperType.toString());
        editTextTitle.getEditText().setText(scraperTypeInformation.title);

        Dialog localResult = builder.create();

        localResult.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
               Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
               button.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {

                       Downloader newDownloader = confirmInput(v);

                        if (newDownloader != null) {
                            dialog.dismiss();
                            StartScrapingDialog dialog = new StartScrapingDialog(newDownloader,
                                    adapter);
                            dialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(),
                                    "auto_download_dialog");
                        }
                   }
               });
            }
        });

        return localResult;
    }

    public Downloader confirmInput(View v) {
        if (!validateField(editTextTitle)) {
            return null;
        }

        JSONObject properties = new JSONObject();

        if ((scraperTypeInformation.scraperType == ScraperType.ICAL)
                || (scraperTypeInformation.scraperType == ScraperType.FACEBOOK_PAGE)) {
            if (!validateField(editTextURL) || !validateFieldForURL(editTextURL)) {
                return null;
            }

            String inputURL = editTextURL.getEditText().getText().toString().trim();

            try {
                properties.put(Downloader.PROPERTY_URL, inputURL);
            }
            catch (Exception e) {
                Log.e(LOG_APP, "Error: " + e.getMessage());
                Log.e(LOG_APP, Log.getStackTraceString(e));
                return null;
            }
        }

        String inputTitle = editTextTitle.getEditText().getText().toString().trim();

        Downloader newDownloader = new Downloader(inputTitle);
        newDownloader.setScraperType(this.scraperTypeInformation.scraperType);
        newDownloader.setProperties(properties);

        // update displayed list
        List<Downloader> downloaders = Model.getInstance().getDownloadersToShow();
        int insertionPosition = getInsertionPosition(inputTitle, downloaders);
        downloaders.add(insertionPosition, newDownloader);
        adapter.refresh();

        // insert into database
        (new InsertDownloaderTask(newDownloader, getContext()))
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return newDownloader;
    }
}
