package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.tasks.EventScraperTask;
import com.kolloware.hypezigapp.tasks.MultiDownloader;
import com.kolloware.hypezigapp.tasks.VoidCallback;

public class MultiDownloadDialog extends AppCompatDialogFragment {

    private DownloadsRecyclerViewAdapter adapter;

    public MultiDownloadDialog(DownloadsRecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_multi_download);
        builder.setMessage(R.string.dialog_multi_download_question);
        builder.setNegativeButton(R.string.dialog_multi_download_no, null);
        builder.setPositiveButton(getString(R.string.dialog_multi_download_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MultiDownloader multiDownloader = new MultiDownloader(
                                Model.getInstance().getDownloadersToShow(), adapter, getContext());
                        multiDownloader.start();
                    }
                });

        Dialog localResult = builder.create();

        return localResult;
    }

}
