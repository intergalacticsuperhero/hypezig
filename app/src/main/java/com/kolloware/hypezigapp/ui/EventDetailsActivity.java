package com.kolloware.hypezigapp.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.net.Scraper;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Map;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;
import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;
import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class EventDetailsActivity extends AppCompatActivity {

    private Event event = null;
    private SimpleDateFormat dateFormat;

    private final static int DEFAULT_DURATION_MILLIS = 90 * 60 * 1000;
    private final static String IMDB_QUERY_URL = "https://m.imdb.com/find?q=";
    private final static String KINO_DE_QUERY_URL = "https://www.kino.de/se/?searchterm=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.event_details_heading);
        event = (Event) getIntent().getExtras().getSerializable("eventData");
        dateFormat = new SimpleDateFormat(getString(R.string.event_details_date_format));

        updateViews();
        (new RefreshEventTask()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void createCalendarEvent(Event inEvent) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".createCalendarEvent() called");

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.Events.TITLE, inEvent.title);
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, inEvent.locationName);
        if (inEvent.eventURL != null && !inEvent.eventURL.isEmpty()) {
            intent.putExtra(CalendarContract.Events.DESCRIPTION, inEvent.eventURL);
        }
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,  inEvent.date.getTime());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, inEvent.date.getTime()
                + DEFAULT_DURATION_MILLIS);
        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);

        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onOptionsItemSelected() called with: item = ["
                + item + "]");

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
            case R.id.item_share:
                shareEvent();
                break;
            case R.id.item_calendar:
                if (event != null) { createCalendarEvent(event); }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void updateViews() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".updateViews() called");

        if (event == null) {
            Log.d(LOG_UI, "No event found.");
            return;
        }

        Log.d(LOG_UI, "Event to show: " + event);

        ((TextView) findViewById(R.id.category)).setText(
                getString(UITools.getCategoryResourceId(event)));
        ((TextView) findViewById(R.id.title)).setText(event.title);

        if (event.subtitle != null && event.subtitle.length() > 0) {
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText(event.subtitle);
            subtitle.setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.subtitle).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.date)).setText(dateFormat.format(event.date));
        TextView location = findViewById(R.id.location);
        if (event.locationURL != null && !event.locationURL.isEmpty()) {
            location.setText(Html.fromHtml("<a href='" + event.locationURL + "'>"
                    + event.locationName.toUpperCase() + "</a>"));
            location.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else {
            location.setText(event.locationName.toUpperCase());
        }


        findViewById(R.id.containerMovie).setVisibility(View.GONE);
        TextView movieLinks = findViewById(R.id.movieLinks);
        if (event.category == Category.CINEMA) {
            try {
                String probableMovieTitle = event.title;
                if (probableMovieTitle.contains(": ")) {
                    // Most events are named like "Sommerkino: The Big Lebowski"
                    // We don't want to look for "Sommerkino: ", so we'll throw that prefix away.
                    probableMovieTitle = probableMovieTitle.substring(
                            probableMovieTitle.lastIndexOf(": ") + 2);
                }
                if (probableMovieTitle.matches(".*\\(.*\\)")) {
                    // Sometimes there are additions like "Deckname Jenny (BRD 2018)"
                    // We'll throw that away, too.
                    probableMovieTitle = probableMovieTitle.substring(0,
                            probableMovieTitle.indexOf("(")).trim();
                }

                String urlEncodedTitle = URLEncoder.encode(probableMovieTitle, "UTF-8");
                String urlIMDB = IMDB_QUERY_URL + urlEncodedTitle;
                String urlKinoDe = KINO_DE_QUERY_URL + urlEncodedTitle;

                movieLinks.setText(Html.fromHtml("<a href='" + urlIMDB+ "'>" +
                        getString(R.string.event_details_movie_imdb) + "</a> | " +
                        "<a href='" + urlKinoDe + "'>" +
                        getString(R.string.event_details_movie_kino_de) + "</a>"));
                movieLinks.setMovementMethod(LinkMovementMethod.getInstance());

                findViewById(R.id.containerMovie).setVisibility(View.VISIBLE);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        TextView readMore = findViewById(R.id.externalLink);
        if (event.eventURL != null && !event.eventURL.isEmpty()) {
            readMore.setText(Html.fromHtml("<a href='" + event.eventURL + "'>"
                    + getString(R.string.event_details_external_link) + "</a>"));
            readMore.setMovementMethod(LinkMovementMethod.getInstance());
            findViewById(R.id.containerWebsite).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.containerWebsite).setVisibility(View.GONE);
        }

        if (event.details != null && event.details.length() > 0) {
            ((TextView) findViewById(R.id.details)).setText(event.details);
            findViewById(R.id.details).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.details).setVisibility(View.GONE);
        }

        String tags = event.tags.toString().replace("[", "")
                .replace("]", "").trim();

        if (tags.length() > 0) {
            ((TextView) findViewById(R.id.tags)).setText(tags);
            findViewById(R.id.containerTags).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.containerTags).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.providerName)).setText(String.format("%s: %s",
                getResources().getString(R.string.event_details_source), event.providerName));

        ImageView imageView = findViewById(R.id.imageView);

        if (event.imageURL != null) {
            new DownloadImageTask(imageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    event.imageURL);
        }
    }


    private class RefreshEventTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(LOG_NET, getClass().getSimpleName()
                    + ".doInBackground() called");

            try {
                AppDatabase database = AppDatabase.getInstance(getApplicationContext());

                if (event.downloaderId != null) {
                    Downloader downloader = database.downloaderDao()
                            .getByDownloaderId(event.downloaderId);

                    ScraperType scraperType = downloader.getScraperType();

                    Class<? extends Scraper> clazz = ScraperType.getScraperClassByType(scraperType);
                    Constructor constructor = clazz.getConstructor(Downloader.class);
                    Scraper scraper = (Scraper) constructor.newInstance(downloader);

                    boolean wasUpdated = scraper.updateEvent(event);
                    if (wasUpdated) {
                        database.eventDao().update(event);
                    }
                }
            }
            catch (Exception e) {
                Log.e(LOG_NET, "Could not scrape additional info for event: " + e.getMessage());
                Log.d(LOG_NET, Log.getStackTraceString(e));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d(LOG_DATA, getClass().getSimpleName()
                    + ".onPostExecute() called with: result = [" + result + "]");
            updateViews();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private static final int THREAD_ID = 10000;

        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            Log.d(LOG_NET, getClass().getSimpleName() + " constructed");

            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            Log.d(LOG_NET, getClass().getSimpleName() + ".doInBackground: called");

            String urldisplay = urls[0];
            Log.v(LOG_NET, "urldisplay = " + urldisplay);

            Map<String, Bitmap> imageCache = Model.getInstance().getImageCache();
            Bitmap mIcon11 = imageCache.get(urldisplay);
            if (mIcon11 != null) {
                Log.v(LOG_NET, "Got image from cache");
                return mIcon11;
            }

            try {
                TrafficStats.setThreadStatsTag(THREAD_ID);
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(in), 1000, 500);
                imageCache.put(urldisplay, mIcon11);
            } catch (Exception e) {
                Log.e(LOG_NET, "doInBackground: ", e);
            }

            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            Log.d(LOG_NET, getClass().getSimpleName() + ".onPostExecute() called with: result = ["
                    + result + "]");
            if (result != null) {
                bmImage.setImageBitmap(result);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateOptionsMenu() called with: menu = ["
                + menu + "]");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return true;
    }

    private void shareEvent() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".shareEvent() called");
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = String.format(getResources().getString(R.string.share_action_message),
                event.title, dateFormat.format(event.date), event.locationName);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, event.title);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent,
                getResources().getString(R.string.share_action_title)));
    }
}
