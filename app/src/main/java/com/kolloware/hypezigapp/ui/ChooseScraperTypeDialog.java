package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.models.ScraperTypeInformation;
import com.kolloware.hypezigapp.net.Scraper;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;
import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class ChooseScraperTypeDialog extends AppCompatDialogFragment {

    private static final String URL_ICAL_FACEBOOK_WIKI =
            "https://gitlab.com/intergalacticsuperhero/hypezig/-/wikis/List-of-other-event-calendars";

    private DownloadsRecyclerViewAdapter adapter;

    public ChooseScraperTypeDialog(DownloadsRecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    protected DownloadsRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateDialog() called with: "
                + "savedInstanceState = [" + savedInstanceState + "]");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.choose_scraper_type_dialog, null);

        builder.setView(view)
                .setTitle(R.string.choose_scraper_type)
                .setNegativeButton(R.string.dialog_button_cancel, null);

        Dialog localResult = builder.create();

        RecyclerView recyclerView = view.findViewById(R.id.scraper_types_recycler_view);
        RecyclerView.Adapter adapter = new ChooseScraperViewAdapter(getScraperTypesToShow());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return localResult;
    }

    private List<ScraperTypeInformation> getScraperTypesToShow() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".getScraperTypesToShow() called");

        List<ScraperTypeInformation> allTypes = ScraperTypeInformation.getDirectory(getContext());

        List<Downloader> existingDownloaders = Model.getInstance().getDownloadersToShow();
        Set<ScraperType> usedScraperTypes = new HashSet<>();
        List<ScraperTypeInformation> typesToShow = new LinkedList<>();
        for (Downloader forDownloader : existingDownloaders) {
            usedScraperTypes.add(forDownloader.getScraperType());
        }

        // Always show iCal & Facebook, even if already used
        usedScraperTypes.remove(ScraperType.ICAL);
        usedScraperTypes.remove(ScraperType.FACEBOOK_PAGE);

        // Don't show already added scraper types
        for (ScraperTypeInformation forScraperTypeInfo : allTypes) {
            if (!usedScraperTypes.contains(forScraperTypeInfo.scraperType)) {
                typesToShow.add(forScraperTypeInfo);
            }
        }

        return typesToShow;
    }


    public class ChooseScraperViewAdapter extends RecyclerView.Adapter {

        List<ScraperTypeInformation> items;

        public ChooseScraperViewAdapter(List<ScraperTypeInformation> items) {
            this.items = items;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateViewHolder() called with: "
                    + "parent = [" + parent + "], viewType = [" + viewType + "]");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scraper_type,
                    parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder h, int position) {
            final ScraperTypeInformation selectedScraper = items.get(position);

            ViewHolder holder = (ViewHolder) h;

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();

                    CreateDownloaderDialog dialog = new CreateDownloaderDialog(selectedScraper,
                            getAdapter());
                    dialog.show(getActivity().getSupportFragmentManager(), "add_downloader_dialog");
                }
            });

            holder.title.setText(selectedScraper.title);
            holder.description.setText(selectedScraper.description);

            if (selectedScraper.url != null) {
                holder.url.setText(Html.fromHtml("<a href='" + selectedScraper.url + "'>"
                        + selectedScraper.url + "</a>"));
                holder.url.setMovementMethod(LinkMovementMethod.getInstance());
            }
            else {
                holder.url.setText(Html.fromHtml("<a href='" + URL_ICAL_FACEBOOK_WIKI + "'>"
                        + "HYPEZIG wiki" + "</a>"));
                holder.url.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public TextView title, description, url;
            public CardView cardView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

                cardView = itemView.findViewById(R.id.card_view);
                title = itemView.findViewById(R.id.scraper_title);
                description = itemView.findViewById(R.id.scraper_description);
                url = itemView.findViewById(R.id.scraper_url);
            }
        }
    }
}
