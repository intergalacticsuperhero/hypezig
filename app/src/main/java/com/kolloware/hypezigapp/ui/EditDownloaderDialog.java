package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.tasks.UpdateDownloaderTask;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class EditDownloaderDialog extends AbstractDownloaderDialog {

    private TextInputLayout editTextTitle, editTextURL;
    private TextView textViewScraperType;

    private Downloader downloader;

    public EditDownloaderDialog(Downloader downloader, DownloadsRecyclerViewAdapter adapter) {
        super(null, adapter);
        this.downloader = downloader;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_downloader_editmask, null);

        builder.setView(view)
                .setTitle(R.string.edit_downloader)
                .setNegativeButton(R.string.dialog_button_cancel, null)
                .setPositiveButton(R.string.dialog_button_okay, null);

        editTextTitle = view.findViewById(R.id.text_input_title);
        editTextTitle.getEditText().setText(downloader.getTitle());

        editTextURL = view.findViewById(R.id.text_input_url);
        if ((downloader.getScraperType() == ScraperType.ICAL) ||
                (downloader.getScraperType() == ScraperType.FACEBOOK_PAGE)) {
            try {
                String url = downloader.getProperties().getString(Downloader.PROPERTY_URL);
                editTextURL.getEditText().setText(url);
            }
            catch (Exception e) {
                Log.e(LOG_UI, "Error: " + e.getMessage());
                Log.e(LOG_UI, Log.getStackTraceString(e));
            }
        }
        else {
            editTextURL.setVisibility(View.GONE);
        }

        textViewScraperType = view.findViewById(R.id.scraper_type);
        textViewScraperType.setText(getString(R.string.edit_downloader_calendar_type) + ": "
                + downloader.getScraperType().toString());

        Dialog localResult = builder.create();

        localResult.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
               Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
               button.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                        if (confirmInput(v)) {
                            dialog.dismiss();
                        }
                   }
               });
            }
        });

        return localResult;
    }


    public boolean confirmInput(View v) {
        if (!validateField(editTextTitle)) {
            return false;
        }

        String inputTitle = editTextTitle.getEditText().getText().toString().trim();

        // update displayed list
        downloader.setTitle(inputTitle);

        if ((downloader.getScraperType() == ScraperType.ICAL)
            || (downloader.getScraperType() == ScraperType.FACEBOOK_PAGE)){
            if (!validateField(editTextURL) || !validateFieldForURL(editTextURL)) {
                return false;
            }

            String inputURL = editTextURL.getEditText().getText().toString().trim();

            try {
                downloader.getProperties().put(Downloader.PROPERTY_URL, inputURL);
            }
            catch (Exception e) {
                Log.e(LOG_UI, "Error: " + e.getMessage());
                Log.e(LOG_UI, Log.getStackTraceString(e));
            }
        }

        adapter.refresh();

        // insert into database
        (new UpdateDownloaderTask(downloader, getContext()))
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return true;
    }
}
