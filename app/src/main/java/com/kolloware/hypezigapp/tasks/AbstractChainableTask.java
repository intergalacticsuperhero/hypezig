package com.kolloware.hypezigapp.tasks;

import android.os.AsyncTask;


public abstract class AbstractChainableTask extends AsyncTask<Void, Void, Void> {

    private ChainableTaskCallback callback;

    public AbstractChainableTask(ChainableTaskCallback callback) {
        this.callback = callback;
    }

    @Override
    protected final void onPostExecute(Void aVoid) {
        onTaskFinished(aVoid);
        callback.notifyPostExecute();
    }

    protected void onTaskFinished(Void aVoid) {};
}
