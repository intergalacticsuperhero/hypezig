package com.kolloware.hypezigapp.tasks;

public interface ChainableTaskCallback {
    void notifyPostExecute();
}
