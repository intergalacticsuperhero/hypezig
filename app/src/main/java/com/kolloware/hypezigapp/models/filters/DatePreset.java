package com.kolloware.hypezigapp.models.filters;

import java.util.Date;

public interface DatePreset {

    Date getDateFrom();
    Date getDateTo();

}
