package com.kolloware.hypezigapp.models;

public enum DownloadStatus {
    INITIALIZING,
    RUNNING,
    FINALIZING,
    SUCCESS,
    ERROR,
    CANCELLED
}
