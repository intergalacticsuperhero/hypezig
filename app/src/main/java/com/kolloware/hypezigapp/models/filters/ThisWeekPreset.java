package com.kolloware.hypezigapp.models.filters;

import java.util.Calendar;
import java.util.Date;

public class ThisWeekPreset implements DatePreset {

    private Date monday, sunday;

    public ThisWeekPreset() {
        Calendar calendar = Calendar.getInstance();

        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }

        monday = calendar.getTime();

        calendar.add(Calendar.DAY_OF_MONTH, 6);

        sunday = calendar.getTime();
    }

    @Override
    public Date getDateFrom() {
        return monday;
    }

    @Override
    public Date getDateTo() {
        return sunday;
    }
}
