package com.kolloware.hypezigapp.models;

public enum Category {
    ART,
    CINEMA,
    CITY,
    CULTURE,
    FAMILY,
    FOOD,
    GUIDED_TOUR,
    LECTURES,
    MISC,
    MUSIC,
    PARTY,
    SHOPPING,
    SPORTS,
    STAGE
}
