package com.kolloware.hypezigapp.models.filters;

import java.util.Calendar;
import java.util.Date;

public class EverythingPreset implements DatePreset {

    private Date today;
    private Date nextTwoMonths;

    public EverythingPreset() {
        Calendar calendar = Calendar.getInstance();

        today = calendar.getTime();

        calendar.add(Calendar.MONTH, 2);

        nextTwoMonths = calendar.getTime();
    }

    @Override
    public Date getDateFrom() {
        return today;
    }

    @Override
    public Date getDateTo() {
        return nextTwoMonths;
    }
}
