package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class Sachsenpunk implements Scraper {
    private static final int THREAD_ID = Sachsenpunk.class.getName().hashCode();
    private static final String SACHSENPUNK_URL = "https://sachsenpunk.de/dates/";
    private static final String SACHSENPUNK_LOCATIONS_URL = "https://sachsenpunk.de/locations/";
    private static final String PROVIDER_NAME = "sachsenpunk.de";

    private static final Pattern REGEX_DATE = Pattern.compile("(\\d{2})\\.(\\d{2})\\. \\(.{2}\\)");
    private static final String DIVIDER = " - ";
    private static final String SEPARATOR_LOCATIONS = "–";
    private static final String SPECIAL_TIME = " Uhr! " + SEPARATOR_LOCATIONS;
    private static final String DEFAULT_TIME = "20:00";
    private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat(
            "dd.MM.yyyy HH:mm");

    private List<Event> result = new ArrayList<>();
    private Map<String, String> locationUrlByName;

    private Downloader downloader;
    private DownloadStatus status = DownloadStatus.SUCCESS;
    private int numLines = 1;
    private int currentLine = 0;
    private Iterator<Element> iterator = null;

    private String currentYear = "2019";
    private String currentDate = "01.01.";

    public Sachsenpunk(Downloader downloader) throws Exception {
        this.downloader = downloader;
    }

    public void init() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".init() called");

        TrafficStats.setThreadStatsTag(THREAD_ID);

        initLocationUrls();

        Log.i(LOG_NET, "fetchEvents: SACHSENPUNK_URL to be read = " + SACHSENPUNK_URL);
        Document doc = Jsoup.parse(new URL(SACHSENPUNK_URL).openStream(),
                "UTF-8", SACHSENPUNK_URL);

        Elements elements = doc.select("article p");

        Log.d(LOG_NET, "Result: " + elements);
        Log.d(LOG_NET, "Count: " + elements.size());

        iterator = elements.iterator();
        numLines = elements.size();
    }

    private void initLocationUrls() throws Exception {
        locationUrlByName = new HashMap<>();

        Log.i(LOG_NET, "fetchEvents:ISO-8859- SACHSENPUNK_LOCATION_URL to be read = " + SACHSENPUNK_LOCATIONS_URL);
        Document doc = Jsoup.parse(new URL(SACHSENPUNK_LOCATIONS_URL).openStream(),
                "UTF-8", SACHSENPUNK_LOCATIONS_URL);

        Elements elements = doc.select("article p a");

        for (Element forElement : elements) {
            // <a href="http://www.altebrauerei-annaberg.de/" target="_blank" rel="noopener">Annaberg-Buchholz – Alte Brauerei</a>
            String locationName = forElement.text();
            if (!locationName.contains(SEPARATOR_LOCATIONS)) continue;

            String url = forElement.attr("href");
            locationUrlByName.put(locationName, url);
        }

        Log.d(LOG_NET, "location map: " + locationUrlByName);
    }

    @Override
    public DownloadStatus getResult() {
        return status;
    }

    @Override
    public List<Event> getEvents() {
        return result;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    private void processMonthIndicator(String inString) {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processMonthIndicator() called with: inString = [" + inString + "]");
        currentYear = inString.split(" ")[2];
        Log.d(LOG_NET, "new year: " + currentYear);
    }

    private void processDateIndicator(String inString) {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processDateIndicator() called with: inString = [" + inString + "]");

        currentDate = inString.substring(0,7);
        Log.d(LOG_NET, "new date: " + currentDate);
    }

    private void processEvent(Node inNode) throws Exception {
        // Leipzig - UT Connewitz - 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film

        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processEvent() called with: inNode = [" + inNode + "]");

        String currentCity = "";

        List<Node> childNodes = inNode.childNodes();

        for (Node forNode : childNodes) {
            if (forNode instanceof Element) { // <b>Dresden</b>
                String forNodeText = ((Element) forNode).text();
                if (forNodeText.length() > 0) currentCity = forNodeText;
            }
            else if (forNode instanceof TextNode) {
                String forNodeText = forNode.toString();
                if (forNodeText.length() < 2) continue;
                if (!forNodeText.contains(SEPARATOR_LOCATIONS)) continue;

                // - UT Connewitz - 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film
                String restOfLine = forNodeText.substring(forNodeText.indexOf(SEPARATOR_LOCATIONS) + 2);

                String locationName, eventText;

                if (restOfLine.contains(SEPARATOR_LOCATIONS)) {
                    // UT Connewitz
                    locationName = restOfLine.substring(0, restOfLine.indexOf(SEPARATOR_LOCATIONS) - 1);
                    Log.d(LOG_NET, "locationName: " + locationName);

                    // 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film
                    eventText = restOfLine.substring(restOfLine.indexOf(SEPARATOR_LOCATIONS) + 2);
                }
                else {
                    locationName = "";
                    eventText = restOfLine;
                }

                String eventTitle = "";
                String eventSubtitle = "";
                String eventTime = "";

                if (eventText.contains(SPECIAL_TIME)) {
                    Log.d(LOG_NET, "Special time info: " + eventText);

                    int position = eventText.indexOf(SPECIAL_TIME);

                    String specialTime;

                    if (position <= 5) {
                        specialTime = eventText.substring(0, eventText.indexOf(SPECIAL_TIME));
                    }
                    else {
                        int posSeparatorBeforeSpecialTime = eventText.substring(0, eventText.indexOf(SPECIAL_TIME))
                                .lastIndexOf(SEPARATOR_LOCATIONS);
                        specialTime = eventText.substring(posSeparatorBeforeSpecialTime + 2, eventText.indexOf(SPECIAL_TIME));

                        String additionalInfo = eventText.substring(0, posSeparatorBeforeSpecialTime - 1);
                        eventSubtitle = additionalInfo;
                    }

                    if (!specialTime.contains(":")) {
                        specialTime += ":00";
                    }

                    eventTitle = eventText.substring(eventText.indexOf(SPECIAL_TIME) + SPECIAL_TIME.length());
                    eventTime = specialTime;
                }
                else {
                    Log.d(LOG_NET, "Default time: " + eventText);

                    eventTitle = eventText;
                    eventTime = DEFAULT_TIME;
                }

                Log.d(LOG_NET, "title: " + eventTitle + ", time: " + eventTime);

                Date eventDate = INPUT_DATE_FORMAT.parse(currentDate + currentYear + " " + eventTime);

                Event newEvent = new Event(eventTitle.trim(),
                        eventSubtitle,
                        "",
                        eventDate,
                        currentCity + ", " + locationName,
                        new ArrayList<String>(),
                        null,
                        Category.MUSIC,
                        PROVIDER_NAME,
                        forNodeText,
                        null,
                        downloader.downloaderId);

                String fullLocationName = currentCity + " " + SEPARATOR_LOCATIONS + " " + locationName;
                if (locationUrlByName.containsKey(fullLocationName)) {
                    newEvent.locationURL = locationUrlByName.get(fullLocationName);
                }

                result.add(newEvent);
            }


        }
    }

    @Override
    public void scrapeNext() throws Exception {
        Element forNode = iterator.next();
        currentLine++;

        /*
        if (!(forNode instanceof TextNode)) {
            return;
        }

        TextNode textNode = (TextNode) forNode;*/

        //String forLine = textNode.text().trim();
        String forLine = forNode.text();

        if (forLine.isEmpty()) {
            return;
        }

        if (forLine.startsWith("————")) {
            // ------------- September 2020 -------------
            processMonthIndicator(forLine);
        }
        else if (REGEX_DATE.matcher(forLine).matches()) {
            // 05.09. (Sa)
            processDateIndicator(forLine);
        }
        else if (forLine.contains(SEPARATOR_LOCATIONS)) {
            // Leipzig - tba. - "Sternburg Fanfest - Deep Shining High + mehr Bands tba.
            processEvent(forNode);
        }
        else {
            Log.d(LOG_NET, "Ignoring: " + currentLine + ": " + forLine);
        }
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentLine / numLines);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }
}
