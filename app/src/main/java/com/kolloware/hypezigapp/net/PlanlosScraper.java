package com.kolloware.hypezigapp.net;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Downloader;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PlanlosScraper extends AbstractICalScraper {

    private static final String PLANLOS_URL = "https://www.planlos-leipzig.org/events.ics";
    private static final String PLANLOS_PROVIDER_NAME = "planlos-leipzig.org";
    private final static Map<String, Category> CATEGORY_BY_LABEL = new HashMap<String, Category>() {{
        put("DISKUSSION", Category.LECTURES);
        put("FILM", Category.CINEMA);
        put("VORTRAG", Category.LECTURES);
        put("INFOSTAND", Category.CITY);
        put("KUNDGEBUNG", Category.CITY);
        put("DEMO", Category.MISC);
        put("KÜFA", Category.FOOD);
        put("SOLIPARTY", Category.PARTY);
        put("KONZERT", Category.MUSIC);
        put("KONGRESS", Category.LECTURES);
    }};

    public PlanlosScraper(Downloader downloader) {
        super(downloader);
    }

    @Override
    protected String getIcalURL() {
        return PLANLOS_URL;
    }

    @Override
    protected String getProviderName() {
        return PLANLOS_PROVIDER_NAME;
    }

    private Category suggestCategoryByKeyword(String text) {
        Category localResult = Category.MISC;

        String upperCaseText = text.toUpperCase();
        for (Map.Entry<String, Category> forSet : CATEGORY_BY_LABEL.entrySet()) {
            if (upperCaseText.contains(forSet.getKey())) {
                return forSet.getValue();
            }
        }

        return localResult;
    }

    @Override
    protected Category getMatchingCategory(JSONObject event) {
        Category suggestion = suggestCategoryByKeyword(event.optString("CATEGORIES"));
        return suggestion;
    }
}
