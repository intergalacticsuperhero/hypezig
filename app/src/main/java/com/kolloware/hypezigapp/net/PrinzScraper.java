package com.kolloware.hypezigapp.net;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;
import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.util.JSoupUtil;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PrinzScraper implements Scraper {
    private static final int THREAD_ID = PrinzScraper.class.getName().hashCode();

    private static final String PROVIDER_NAME = "prinz.de";

    private static final SimpleDateFormat URL_DATE_FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd");

    private static final SimpleDateFormat EVENT_DATETIME_FORMAT = new SimpleDateFormat(
            "dd. MM. yyyy, HH:mm", Locale.GERMANY);
    private static final String ELEMENT_EVENTS = "article.event";
    private static final int NUM_DAYS_TO_SCRAPE = 30;


    private Iterator<String> dateIterator;
    private int progressCounter = 0;

    private List<Event> localResult = new ArrayList<>();
    private DownloadStatus localDownloadStatus = DownloadStatus.RUNNING;
    private Downloader downloader;


    public PrinzScraper(Downloader downloader) throws Exception {
       this.downloader = downloader;
    }

    private Elements getEventsByPath(String inPath) throws Exception {
        String url = "https://prinz.de/leipzig/events/" + inPath;

        Log.i(LOG_NET, "fetchEvents: URL to be read = " + url);

        Connection connection = Jsoup.connect(url);
        connection.maxBodySize(0);
        Connection.Response response = connection.timeout(2 * 60 * 1000).execute();

        Document doc = Jsoup.parse(response.body());

        return doc.select(ELEMENT_EVENTS);
    }

    public void init() throws Exception {

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Calendar cal = Calendar.getInstance();

        List<String> allDatesToParse = new ArrayList<>();

        // Get next 30 days
        for (int i = 0; i < NUM_DAYS_TO_SCRAPE; i++) {
            String parsedDate = URL_DATE_FORMAT.format(cal.getTime());
            allDatesToParse.add(parsedDate);
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }

        dateIterator = allDatesToParse.iterator();
    }

    private final static Map<String, Category> CATEGORY_MAP = new HashMap<String, Category>() {{
        put("KINDER & FAMILIE", Category.FAMILY);
        put("SPORT", Category.SPORTS);
        put("PARTY", Category.PARTY);
        put("KONZERTE & LIVEMUSIK", Category.MUSIC);
        put("FESTIVALS & OPEN-AIRS", Category.MUSIC);
        put("FÜHRUNGEN", Category.GUIDED_TOUR);
        put("ESSEN & TRINKEN", Category.FOOD);
        put("AUSSTELLUNGEN", Category.ART);
        put("FILM & KINO", Category.CINEMA);
        put("BÜHNE", Category.STAGE);
        put("ONLINE & TV & Radio", Category.CULTURE);
        put("STADTLEBEN", Category.CITY);
        put("MÄRKTE", Category.SHOPPING);
        put("SPECIAL EVENTS", Category.MISC);
        put("MESSEN & KONGRESSE", Category.MISC);
    }};

    private final static Map<String, String> MONTH_LABEL_TO_NUMERAL = new HashMap<String, String>() {{
        put("Jan", "01");
        put("Feb", "02");
        put("Mrz", "03");
        put("Apr", "04");
        put("Mai", "05");
        put("Jun", "06");
        put("Jul", "07");
        put("Aug", "08");
        put("Sep", "09");
        put("Okt", "10");
        put("Nov", "11");
        put("Dez", "12");
    }};


    @Override
    public DownloadStatus getResult() {
        return localDownloadStatus;
    }

    @Override
    public List<Event> getEvents() {
        return localResult;
    }

    @Override
    public boolean hasNext() {
        if (dateIterator.hasNext()) {
            return true;
        }
        else {
            if (localDownloadStatus == DownloadStatus.RUNNING) {
                localDownloadStatus = DownloadStatus.SUCCESS;
            }
            return false;
        }
    }

    private Event scrapeEvent(Element event) throws Exception {
        Log.d(LOG_NET, "scraping: " + event.text());

        String title = event.select(".prinz-event-teaser-title").text();

        String locationName = event.select(".prinz-event-teaser-meta-location").text();

        String imageCSS = event.select("figure").attr("style");
        String imageURL = imageCSS.replace("background-image: url('", "")
                .replace("')", "");

        String categoryText = event.select(".prinz-event-teaser-category").text();
        if (categoryText.contains(",")) {
            categoryText = categoryText.substring(categoryText.indexOf(",") + 2, categoryText.length());
        }

        Category category = Category.MISC;
        if (CATEGORY_MAP.containsKey(categoryText)) {
            category = CATEGORY_MAP.get(categoryText);
        }

        String id = event.attr("id")
                .replace("prinz-event-teaser-", "");

        String eventURL = event.select(".prinz-stretched-link").attr("href");

        String eventTimeDay = event.select(".prinz-event-teaser-date-day").text();
        if (eventTimeDay.length() < 3) { eventTimeDay = "0" + eventTimeDay; }
        String eventTimeMonthAsString = event.select(".prinz-event-teaser-date-month").text();
        String eventTimeMonthAsNumeral = MONTH_LABEL_TO_NUMERAL.get(eventTimeMonthAsString);
        String eventTime = event.select(".prinz-event-teaser-date-time").text();
        eventTime = eventTime.substring(eventTime.indexOf(" ") + 1, eventTime.indexOf(" Uhr"));

        Calendar now = Calendar.getInstance();
        int thisYear = now.get(Calendar.YEAR);

        String parseableDateString = eventTimeDay + " " + eventTimeMonthAsNumeral + ". " + thisYear
                + ", " + eventTime;

        Date eventDate = EVENT_DATETIME_FORMAT.parse(parseableDateString);
        Calendar eventDateAsCalendar = Calendar.getInstance();
        eventDateAsCalendar.setTime(eventDate);
        if (eventDateAsCalendar.get(Calendar.MONTH) < now.get(Calendar.MONTH)) {
            eventDateAsCalendar.add(Calendar.YEAR, 1);
            eventDate = eventDateAsCalendar.getTime();
        }

        String prinzId = parseableDateString + "-" + id;

        Event newEvent = new Event(title,
                null,
                null,
                eventDate,
                locationName,
                new ArrayList<>(),
                (imageURL.length() > 0) ? imageURL : null,
                category,
                PROVIDER_NAME,
                prinzId,
                categoryText,
                downloader.downloaderId);

        newEvent.eventURL = eventURL;

        Log.d(LOG_APP, "New event: " + newEvent);

        return newEvent;
    }

    @Override
    public void scrapeNext() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".scrapeNext() called");

        String date = dateIterator.next();
        Log.d(LOG_NET, "scraping events for: " + date);

        Elements newElements = getEventsByPath(date);
        Log.d(LOG_NET, "numEvents " + date + ": " + newElements.size());

        for (Element forElement : newElements) {
            Event newEvent = scrapeEvent(forElement);
            localResult.add(newEvent);
        }

        progressCounter++;
    }

    @Override
    public int getProgress() {
        return  (int) (100.0 * progressCounter / NUM_DAYS_TO_SCRAPE);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".scrapeAdditionalInformation() called "
                + "with: inOutEvent = [" + inOutEvent + "]");

        if (inOutEvent.details != null && !inOutEvent.details.isEmpty()) {
            // No update needed
            return false;
        }
        else {
            TrafficStats.setThreadStatsTag(THREAD_ID);

            Document doc = JSoupUtil.getDocumentFromURL(inOutEvent.eventURL);

            String details = doc.select(".prinz-text").text();
            String locationURL = doc.select("address div[itemprop='name'] a").attr("href");

            inOutEvent.details = details;
            inOutEvent.locationURL = locationURL;

            return true;
        }
    }
}

