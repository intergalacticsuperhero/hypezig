package com.kolloware.hypezigapp.net;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KreuzerScraper implements Scraper {
    private static final int THREAD_ID = 40000;

    private static final String PROVIDER_NAME = "kreuzer-leipzig.de";
    private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat(
            "dd.MM.yyyy HH:mm");
    private static final SimpleDateFormat ID_DATE_FORMAT = new SimpleDateFormat(
            "yyyyMMddHHmm");

    private final Pattern patternDate = Pattern.compile("(\\d{2})\\.(\\d{2})\\.");
    private final Pattern patternTime = Pattern.compile("(\\d{2})\\:(\\d{2})");
    private final String thisYear = (new SimpleDateFormat("yyyy"))
            .format(Calendar.getInstance().getTime());

    private JSONObject jsonResponse;
    private Iterator<CategoryEventKey> categoryEventKeyIterator;
    private int numEvents = 1;
    private int eventCounter = 0;

    private List<Event> localResult = new ArrayList<>();

    private Downloader downloader;
    private DownloadStatus localDownloadStatus = DownloadStatus.RUNNING;

    public KreuzerScraper(Downloader downloader) throws Exception {
        this.downloader = downloader;
    }

    private final static Map<String, Category> CATEGORY_MAPPING = new HashMap<String, Category>() {{
        put("theater", Category.STAGE);
        put("film", Category.CINEMA);
        put("musik", Category.MUSIC);
        put("clubbing", Category.PARTY);
        put("kunst", Category.ART);
        put("literatur", Category.LECTURES);
        put("vortraege-diskussionen", Category.LECTURES);
        put("etc", Category.MISC);
        put("kinder-familie", Category.FAMILY);
        put("umland", Category.MISC);
        put("gastro-events", Category.FOOD);
        put("lokale-radios", Category.CULTURE);
        put("natur-umwelt", Category.MISC);
        put("spiel", Category.MISC);
    }};

    private static Category getCategoryByString(String inCategoryName) {
        Category localResult = CATEGORY_MAPPING.get(inCategoryName);
        if (localResult == null) {
            Log.e(LOG_NET, "No mapping for category: " + inCategoryName);
            return Category.MISC;
        }
        return localResult;
    }

    public void init() throws Exception {
        Log.d(LOG_NET, KreuzerScraper.class.getName() + ".init() called");

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Date today = Calendar.getInstance().getTime();

        Calendar calendarNextMonth = Calendar.getInstance();
        calendarNextMonth.add(Calendar.MONTH, 2);
        Date nextMonth = calendarNextMonth.getTime();

        SimpleDateFormat searchDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            String url = "https://kreuzer-leipzig.de/termine/filter"
                    + "?datestart="
                    + searchDateFormat.format(today)
                    + "&dateend="
                    + searchDateFormat.format(nextMonth);

            Log.i(LOG_NET, "init: URL to be read = " + url);

            Connection connection = Jsoup.connect(url);
            connection.maxBodySize(0);
            Document doc = connection.timeout(2 * 60 * 1000).ignoreContentType(true).get();

            String jsonText = doc.body().text();
            jsonResponse = new JSONObject(jsonText);

            JSONArray names = jsonResponse.names();
            LinkedList<CategoryEventKey> categoryEventKeyList = new LinkedList<>();

            Log.i(LOG_NET, "fetchEvents: " + names.length() + " categories found.");
            for (int i = 0; i < names.length(); i++)
            {
                String forCategoryName = names.getString(i);
                JSONObject forCategoryObject = jsonResponse.getJSONObject(forCategoryName);
                JSONArray events = forCategoryObject.getJSONArray("events");
                for (int j = 0; j < events.length(); j++) {
                    categoryEventKeyList.add(new CategoryEventKey(forCategoryName, j));
                }
            }
            Log.i(LOG_NET, "fetchEvents: " + categoryEventKeyList.size() + " events found.");

            numEvents = categoryEventKeyList.size();

            categoryEventKeyIterator = categoryEventKeyList.iterator();
        }
        catch(Exception e) {
            Log.e(LOG_NET, "init: ", e);
            throw(e);
        }
    }

    @Override
    public DownloadStatus getResult() { return localDownloadStatus; }

    @Override
    public List<Event> getEvents() { return localResult; }

    @Override
    public boolean hasNext() {
        if (categoryEventKeyIterator.hasNext()) {
            return true;
        }
        else {
            if (localDownloadStatus == DownloadStatus.RUNNING) {
                localDownloadStatus = DownloadStatus.SUCCESS;
            }
            return false;
        }
    }

    @Override
    public void scrapeNext() throws Exception {
        Log.d(LOG_NET, "fetchEvents: Processing event " + eventCounter + "...");

        try {
            eventCounter++;

            CategoryEventKey forKey = categoryEventKeyIterator.next();

            JSONObject forCategoryJSONObject = jsonResponse.getJSONObject(forKey.categoryName);
            JSONObject forEventJSONObject = forCategoryJSONObject.getJSONArray("events")
                    .getJSONObject(forKey.eventNum);

            String title = forEventJSONObject.getString("title");

            String subtitle = forEventJSONObject.getString("info");
            String details = forEventJSONObject.getString("text");

            JSONArray dates = forEventJSONObject.getJSONArray("dates");

            String imageURL = forEventJSONObject.getString("thumb");
            imageURL = !imageURL.isEmpty() ? "https://kreuzer-leipzig.de" + imageURL : null;

            boolean isTipp = forEventJSONObject.getBoolean("tip");
            boolean isNew = forEventJSONObject.getBoolean("new");

            LinkedList<String> tags = new LinkedList<>();
            if (isTipp) { tags.add("Tipp"); }
            if (isNew) { tags.add("Neu"); }

            for (int i = 0; i < dates.length(); i++) {
                JSONObject forDate = dates.getJSONObject(i);

                JSONObject forVenue = forDate.getJSONObject("venue");

                String locationName = forVenue.getString("name");
                String locationURL = forVenue.getString("slug");

                JSONArray lines = forDate.getJSONArray("lines");
                for (int j = 0; j < lines.length(); j++) {
                    JSONObject forLine = lines.getJSONObject(j);

                    String dateLabel = forLine.getString("date");
                    String timeLabel = forLine.getString("time");
                    Matcher m = patternDate.matcher(dateLabel);

                    String dateAsString = "";
                    while (m.find()) {
                        dateAsString = m.group() + thisYear;
                    }

                    m = patternTime.matcher(timeLabel);

                    while (m.find()) {
                        Date eventDate = INPUT_DATE_FORMAT.parse(dateAsString + " "
                                + m.group());

                        String providerId = forKey.categoryName + "_" + title + "_" +
                                ID_DATE_FORMAT.format(eventDate);

                        Category category = getCategoryByString(forKey.categoryName);

                        Event newEvent = new Event(title, subtitle, details, eventDate,
                                locationName, tags, imageURL, category, PROVIDER_NAME,
                                providerId, forKey.categoryName, downloader.downloaderId);

                        newEvent.locationURL = locationURL.isEmpty() ? null :
                                "https://kreuzer-leipzig.de/adressen/" + locationURL;

                        Log.d(LOG_NET, "fetchEvents: new event created: " + newEvent);

                        localResult.add(newEvent);
                    }
                }
            }
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error while scraping: " + e.getMessage());
            e.printStackTrace();
            localDownloadStatus = DownloadStatus.ERROR;
            throw(e);
        }
    }

    @Override
    public int getProgress() {
        return  (int) (100.0 * eventCounter / numEvents);
    }

    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }

    private class CategoryEventKey {
        String categoryName;
        int eventNum;

        public CategoryEventKey(String inCategoryName, int inEventNum) {
            this.categoryName = inCategoryName;
            this.eventNum = inEventNum;
        }
    }
}
