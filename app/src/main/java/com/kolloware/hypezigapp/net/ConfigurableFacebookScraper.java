package com.kolloware.hypezigapp.net;

import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Downloader;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class ConfigurableFacebookScraper extends AbstractFacebookScraper {

    private static final String BASE_URL_MOBILE_PAGE = "https://m.facebook.com";

    public ConfigurableFacebookScraper(Downloader downloader) throws Exception {
        super(downloader);
    }

    @Override
    protected String getFacebookPageUrl() {
        try {
            String userUrl = downloader.getProperties().getString(Downloader.PROPERTY_URL);
            String withoutProtocol = userUrl.replace("http://", "")
                    .replace("https://", "");
            String withoutBaseUrl = withoutProtocol.substring(withoutProtocol.indexOf("/"));
            String resultUrl = BASE_URL_MOBILE_PAGE + withoutBaseUrl;

            return resultUrl;
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.e(LOG_NET, Log.getStackTraceString(e));
        }

        return null;
    }

    @Override
    protected Category getDefaultCategory() {
        return Category.MISC;
    }
}
