package com.kolloware.hypezigapp.util;

import android.util.Log;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class JSoupUtil {

    private final static int DEFAULT_TIMEOUT = 2 * 60 * 1000;

    public static Document getDocumentFromURL(String inURL) throws Exception {
        Log.d(LOG_NET, JSoupUtil.class.getSimpleName() + ".getDocumentFromURL() called with: inURL = [" + inURL + "]");

        Connection connection = Jsoup.connect(inURL);
        connection.maxBodySize(0);
        Document doc = connection.timeout(DEFAULT_TIMEOUT).get();

        return doc;
    }
}
