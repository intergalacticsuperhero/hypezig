package com.kolloware.hypezigapp.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.tasks.MultiDownloader;
import com.kolloware.hypezigapp.ui.OptionalUICallback;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;

public class AutomaticDownloadReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_APP, getClass().getSimpleName() + ".onReceive() called with: context = ["
                + context + "], intent = [" + intent + "]");

        MultiDownloader task = new MultiDownloader(
                Model.getInstance().getDownloadersToShow(),
                new OptionalUICallback(),
                context);
        task.start();
    }
}
