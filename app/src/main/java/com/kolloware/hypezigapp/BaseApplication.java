package com.kolloware.hypezigapp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.kolloware.hypezigapp.util.AutomaticDownloadReceiver;
import com.kolloware.hypezigapp.util.DailyAlarmReceiver;

import java.util.Date;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class BaseApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String CHANNEL_FAVORITES_ID = "favorites_reminder";

    /**
     * Logging tag used for common UI lifecycle events
     */
    public static final String LOG_UI = "UI";

    /**
     * Logging tag used for any kind of network I/O communication
     */
    public static final String LOG_NET = "NET";

    /**
     * Logging tag used for storage; local files, preferences and databases
     */
    public static final String LOG_DATA = "DATA";

    /**
     * Logging tag used for business logic and app related things not
     * already covered by the other log tags
     */
    public static final String LOG_APP = "APP";

    private static final int REQUEST_TIMER_DAILY = 1;
    private static final int REQUEST_AUTOMATIC_DOWNLOAD = 2;

    private static final String PREFERENCE_SYNC = "sync";
    private static final String PREFERENCE_SYNC_CYCLE = "sync_cycle";
    private static final String SYNC_CYCLE_DAILY = "daily";
    private static final String SYNC_CYCLE_WEEKLY = "weekly";
    private static final String SYNC_CYCLE_TWO_WEEKS = "two_weeks";

    @Override
    public void onCreate() {
        Log.d(LOG_APP, "BaseApplication.onCreate() called");

        super.onCreate();

        initLogging();
        createNotificationChannels();
        initAlarmReceiver();
        initAutoUpdater();

        initPreferenceChangeListener();
    }

    private void initLogging() {
        Log.i(LOG_APP, "Creating Application");

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " created");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " started");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " resumed");
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " paused");
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " stopped");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " saved");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Log.i(LOG_UI, activity.getClass().getSimpleName() + " destroyed");
            }
        });

        if (BuildConfig.DEBUG) {
            StrictMode.enableDefaults();
        }
    }


    private void createNotificationChannels() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".createNotificationChannels() called");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_FAVORITES_ID,
                    getString(R.string.notification_info_title),
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel1.setDescription(getString(R.string.notification_info_description));

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
        }
    }


    private void initAlarmReceiver() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".initAlarmReceiver() called");

        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent dailyAlarmIntent = new Intent(this, DailyAlarmReceiver.class);
        PendingIntent pendingDailyAlarmIntent = PendingIntent.getBroadcast(this,
                REQUEST_TIMER_DAILY, dailyAlarmIntent, FLAG_UPDATE_CURRENT);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                AlarmManager.INTERVAL_DAY, pendingDailyAlarmIntent);
    }

    public void initAutoUpdater() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".initAutoUpdater() called");

        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isUpdateActive = prefs.getBoolean(PREFERENCE_SYNC, false);
        String syncCycle = prefs.getString(PREFERENCE_SYNC_CYCLE, SYNC_CYCLE_DAILY);

        Intent automaticDownloaderIntent = new Intent(this, AutomaticDownloadReceiver.class);
        PendingIntent pendingAutomaticDownloaderIntent = PendingIntent.getBroadcast(this,
                REQUEST_AUTOMATIC_DOWNLOAD, automaticDownloaderIntent, FLAG_UPDATE_CURRENT);

        if (isUpdateActive) {
            long millis;

            switch (syncCycle) {
                case SYNC_CYCLE_WEEKLY:
                    millis = AlarmManager.INTERVAL_DAY * 7;
                    break;
                case SYNC_CYCLE_TWO_WEEKS:
                    millis = AlarmManager.INTERVAL_DAY * 14;
                    break;
                default:
                    millis = AlarmManager.INTERVAL_DAY;
            }

            manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + millis,
                    millis, pendingAutomaticDownloaderIntent);
        }
        else {
            pendingAutomaticDownloaderIntent.cancel();
        }
    }

    private void initPreferenceChangeListener() {
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.initAutoUpdater();
    }
}
